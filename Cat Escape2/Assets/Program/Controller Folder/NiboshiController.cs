﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NiboshiController : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        this.player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.07f, 0, 0);
        if (transform.position.x < -10)
        {
            Destroy(gameObject);
        }

        Vector2 p = transform.position;
        Vector2 h = this.player.transform.position;
        Vector2 t = p - h;
        float d = t.magnitude;
        float a = 0.5f;
        float a2 = 1.0f;

        if (d < a + a2)
        {
            GameObject director = GameObject.Find("GameDirector1");
            director.GetComponent<GameDirector1>().IncreaseCo();

            Destroy(gameObject);
        }
    }
}

