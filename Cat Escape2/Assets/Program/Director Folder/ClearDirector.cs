﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ClearDirector : MonoBehaviour
{
    float startTime;
    // Start is called before the first frame update
    void Start()
    {
        if(Time.timeSinceLevelLoad > 60.0f)
        {
            startTime = Time.time;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time - startTime > 60.0f)
        {
            SceneManager.LoadScene("ClearScene");
        }
    }
}
