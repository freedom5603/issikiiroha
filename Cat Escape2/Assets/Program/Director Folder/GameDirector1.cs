﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector1 : MonoBehaviour
{
    public int Co = 0;
    public GameObject LifeGauge;
    public GameObject Counter = null;
    // Start is called before the first frame update
    void Start()
    {
        this.LifeGauge = GameObject.Find("LifeGauge");
    }

    // Update is called once per frame
    public void IncreaseCo()
    {
        Co++;
    }

    void Update()
    {
        Text arrowText = this.Counter.GetComponent<Text>();
        arrowText.text = "スコア：" + Co;
    }
}
