﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOcerDirector : MonoBehaviour
{
    GameObject LifeGauge;
    // Start is called before the first frame update
    void Start()
    {
        this.LifeGauge = GameObject.Find("LifeGauge");
    }

    // Update is called once per frame
    void Update()
    {
        if(this.LifeGauge.GetComponent<Image>().fillAmount < 0.01f)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
