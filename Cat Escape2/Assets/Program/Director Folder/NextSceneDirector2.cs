﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextSceneDirector2 : MonoBehaviour
{
    float startTime;
    // Start is called before the first frame update
    void Start()
    {
        if (Time.timeSinceLevelLoad > 40.0f)
        {
            startTime = Time.time;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - startTime > 40.0f)
        {
            SceneManager.LoadScene("GameScene");
        }
    }
}
