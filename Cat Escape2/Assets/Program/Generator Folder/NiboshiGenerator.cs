﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NiboshiGenerator : MonoBehaviour
{
    public GameObject NiboshiPrefab;
    float spen = 4.1f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.spen)
        {
            this.delta = 0;
            GameObject go = Instantiate(NiboshiPrefab) as GameObject;
            int px = Random.Range(-3, 4);
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}