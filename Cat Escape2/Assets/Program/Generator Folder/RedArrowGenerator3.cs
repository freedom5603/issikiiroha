﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedArrowGenerator3 : MonoBehaviour
{
    public GameObject arrowPrefab;
    float spen = 1.6f;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.spen)
        {
            this.delta = 0;
            GameObject go = Instantiate(arrowPrefab) as GameObject;
            int px = Random.Range(-3, 4);
            go.transform.position = new Vector3(10, px, 0);
        }
    }
}

