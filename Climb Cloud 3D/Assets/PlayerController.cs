﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 20;   //動く速さ
    Rigidbody rigid;
    public float jumpSpeed;
    private bool isJumping = false;
    // Start is called before the first frame update
    void Start()
    {
        this.rigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space) && this.rigid.velocity.y == 0)
        {
            rigid.velocity = Vector3.up * jumpSpeed;
            isJumping = true;
        }
        //カーソルキーの入力を所得
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");

        //カーソルキーの入力に合わせて移動方向を設定
        var movement = new Vector3(moveHorizontal, 0, moveVertical);

        //Rigidbodyに力を与えて球を動かす
        rigid.AddForce(movement * speed);

    }
}
