﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDirector : MonoBehaviour
{
    public GameObject BombPrefab;
    int x = 0;
    int y = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.Translate(-1, 0, 0);
            x--;
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.Translate(1, 0, 0);
            x++;
        }

        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            transform.Translate(0, 1, 0);
            y++;
        }

        if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            transform.Translate(0, -1, 0);
            y--;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bomb = Instantiate(BombPrefab) as GameObject;
            bomb.transform.position = new Vector3(x, y, 0);
        }
    }
}
